import Vue from 'vue'

//时间格式过滤
Vue.filter('getTimer',(setTimer)=>{
             let date = setTimer ? new Date(setTimer) : new Date()
		  	 var y = date.getFullYear()   //年
		  	 var m = date.getMonth() + 1  //月
		  	 m = m < 10 ? ('0' + m) : m; 
		  	 var d = date.getDate()     //日
		  	 d = d < 10 ? ('0' + d) : d   
		  	 var h = date.getHours()    //时
             h = h < 10 ? ('0' + h) : h  
             var minute = date.getMinutes();    
    		 var second = date.getSeconds();
		  	 const newDate = y+"-"+m+"-"+d+" "+h+":"+minute+":"+second
		  	 return newDate
})