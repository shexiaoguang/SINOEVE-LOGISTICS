//时间格式化
export function getTimer(setTimer){
		  	 let date = setTimer ? new Date(setTimer) : new Date()
		  	 var y = date.getFullYear()   //年
		  	 var m = date.getMonth() + 1  //月
		  	 m = m < 10 ? ('0' + m) : m; 
		  	 var d = date.getDate()     //日
		  	 d = d < 10 ? ('0' + d) : d   
		  	 var h = date.getHours()    //时
             h = h < 10 ? ('0' + h) : h  
             var minute = date.getMinutes();    
    		 var second = date.getSeconds();
		  	 const newDate = y+"-"+m+"-"+d+" "+h+":"+minute+":"+second
		  	 return newDate
}
//输入框节流
export function debounce(fun,delay){
	let timer = null;
	return (...args) => {
        if (timer) {
            clearTimeout(timer)
        }
        timer = setTimeout(() => {
            fun.apply(this, args)
        }, delay)
       }
}


export default {
	getTimer
}
