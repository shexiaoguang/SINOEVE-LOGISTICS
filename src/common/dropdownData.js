
	//合作类型数据
export function cooperationType(){
		return [
			{value:"不限",label:"不限",name:"unlimited"},
			{value:"客户",label:"客户",name:"client"},
			{value:"承运商",label:"承运商",name:"carrier"}
		]
	}
	//合作性质
export function cooperativeNature(){
		return [
			{value:"不限",label:"不限",name:"unlimited"},
			{value:"长期",label:"长期",name:"client"},
			{value:"临时",label:"临时",name:"temporary"},
			{value:"合同",label:"合同",name:"contract"},
			{value:"普通客户",label:"普通客户",name:"ordinary"},
			{value:"重要客户",label:"重要客户",name:"significance"},
			{value:"VIP客户",label:"VIP客户",name:"VIP"},
			{value:"零散客户",label:"零散客户",name:"scattered"}
		]
	}
	//合作状态
export function stateCooperation(){
		return [
		   {value:"有效",label:"有效",name:"yes"},
		   {value:"无效",label:"无效",name:"no"} 
		]
	}

export default {
	cooperationType,
	cooperativeNature,
	stateCooperation
}
