import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import mutations from './mutations'
Vue.use(Vuex)

////当用户浏览器有设置隐身模式时,如果不加try{}catch(){} 会报错,导致无法程序无法运行
//let defaultCity = "深圳"
//try{
//	if(localStorage.city){
//		defaultCity = localStorage.city
//	}
//}catch (e) {}

export default new Vuex.Store({
	//存放全局公用的数据
//	state:{
////		city: localStorage.city || "深圳"
//      city:defaultCity
//	},
   state,
//	actions:{
//		//接收组件传递的参数
//		changeCity(ctx,city){
//			console.log(ctx)
//			console.log(city)
//			ctx.commit('changeCity',city)   //调用mutations
//		}
//	},
	mutations,
})
