//当用户浏览器有设置隐身模式时,如果不加try{}catch(){} 会报错,导致无法程序无法运行
let defaultUser = "中永物流"
let isRouterAlive = true
try{
	if(sessionStorage.user){
		defaultUser = JSON.parse(decodeURI(sessionStorage.user))
	}
}catch (e) {}

//导出内容
export default {
	user:defaultUser,
	isRouterAlive
}
