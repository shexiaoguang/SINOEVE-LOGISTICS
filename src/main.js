// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import './assets/iconfont.css'
import './assets/reset.css'
import 'babel-polyfill'   //判断是否包里是有promise ,自动添加ES6新特性
import store from './store'
//element-ui完整引入
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import echarts from 'echarts';
import VeLine from 'v-charts/lib/line'
import VePie from 'v-charts/lib/pie'
Vue.component(VeLine.name,VeLine)
Vue.component(VePie.name,VePie)
Vue.prototype.$echarts=echarts
Vue.use(ElementUI);

import common from './common/commons'
Vue.prototype.common = common
//axios,vue-axios引入
import axios from 'axios'
import VueAxios from 'vue-axios'
// axios.defaults.baseURL = "http://192.168.0.101:9001"   
Vue.prototype.$http = axios
Vue.prototype.baseURL = process.env.API_ROOT
Vue.use(VueAxios,axios)

//md5加密
import md5 from 'js-md5'
Vue.prototype.$md5= md5
// const $api="http://192.168.0.101:9001"

Vue.config.productionTip = false

router.beforeEach((to,from,next)=>{
	 if(to.path === '/login') {
	    sessionStorage.removeItem('user');
	  }
	  let user = JSON.parse(sessionStorage.getItem('user'));
	  if (!user && to.path !== '/login') {
	    next({ path: '/login' })
	  } else {
	    next()
	  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
