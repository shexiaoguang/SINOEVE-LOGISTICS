import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/mains/Login/Login'
import Home from '@/mains/Home/Home'
import NotFound from '@/mains/404'


Vue.use(Router)

export default new Router({
  routes: [
    {path: '/login',name: 'Login',component: Login,},
	  {path: '/404',component: NotFound,name: '404',hidden: true},
    {
    	name:'首页',
    	path:'/',
    	redirect:'/main',  //设置默认路线
    	component: Home,
    	children:[
    		{ path: '/main', component:()=> import ('@/page/Main/index'), name: '首页', hidden: true }
    	]
    },
	  {
     	 name: '订单',
     	 path:'/order',
     	 component: Home,
     	 children:[
           { path: 'mangement', component:()=> import ('@/page/Order/OrderMangement/index'), name: '指令管理' },
           { path: 'control', component:()=>import('@/page/Order/OrderControl/index'), name: '订单管理' },
           { path: 'dispatch', component:()=> import ('@/page/Order/OrderDispatch/index'), name: '订单调度' },
           { path: 'trace', component:()=>import('@/page/Order/OrderTrace/index'), name: '订单跟踪' },
           { path: 'enquiry', component:()=>import('@/page/Order/OrderEnquiry/index'), name: '订单询价' },
           { path: 'import', component:()=>import('@/page/Order/OrderImport/index'), name: '订单导入' },
           { path: 'receipt', component:()=>import('@/page/Order/OrderReceipt/index'), name: '正本回单' },
           { path: 'tracking', component:()=>import('@/page/Order/BatchTracking/index'), name: '批量跟踪' },
           { path: 'billing', component:()=>import('@/page/Order/OrderBilling/index'), name: '在线开单' },
           { path: 'acceptshipper', component:()=>import('@/page/Order/AcceptShipper/index'), name: '收发货方开单' },
           { path: 'batchscheduling', component:()=>import('@/page/Order/BatchScheduling/index'), name: '批量调度' }
     	 ],
     	 meta: { title: 'order' }
    },
    {
     	 name: '地图',
     	 path:'/map',
     	 component: Home,
     	 children:[
     	     { path: 'driverposition', component: ()=>import('@/page/Map/DriverPosition/index'), name: '司机定位'},
           { path: 'linetrack', component: ()=>import('@/page/Map/LineTracking/index'), name: '线路跟踪' },
           { path: 'scheduling', component:()=>import('@/page/Map/ScheDuling/index'), name: '地图调度' },
           { path: 'trackorders', component: ()=>import('@/page/Map/TrackOrders/index'), name: '订单轨迹' },
           { path: 'trackposition', component: ()=>import('@/page/Map/TrackPosition/index'), name: '订单定位' }
     	 ],
     	 meta: { title: 'map' }
    },
    {
     	 name: '财务',
     	 path:'/finance',
     	 component: Home,
     	 children:[
     	     //收支管理
     	     { path: 'income', component: ()=>import('@/page/Finace/income/Income/index'), name: '收入核算'},
           { path: 'expenditure', component: ()=>import('@/page/Finace/income/Expenditure/index'), name: '支出核算' },
           { path: 'invoice', component: ()=>import('@/page/Finace/income/Invoice/index'), name: '开票管理' },
           { path: 'accountant', component: ()=>import('@/page/Finace/income/Accountant/index'), name: '会计台帐' },
           { path: 'payment', component: ()=>import('@/page/Finace/income/Payment/index'), name: '付款申请清单' },
           { path: 'runWater', component: ()=>import('@/page/Finace/income/RunWater/index'), name: '财务流水' },
           //账单管理
           { path: 'customerbill', component:()=>import('@/page/Finace/bill/CustomerBill/index'), name: '客户账单'},
           { path: 'customerpay', component: ()=>import('@/page/Finace/bill/CustomerPay/index'), name: '客户先付' },
           { path: 'customertopay', component: ()=>import('@/page/Finace/bill/CustomerToPay/index'), name: '客户到付' },
           { path: 'carrierBill', component: ()=>import('@/page/Finace/bill/CarrierBill/index'), name: '承运商账单' },
           { path: 'carrierpay', component: ()=>import('@/page/Finace/bill/CarrierPay/index'), name: '承运商现付' },
           { path: 'carriertopay', component: ()=>import('@/page/Finace/bill/CarrierToPay/index'), name: '承运商到付' },
           { path: 'driverbill', component:()=>import('@/page/Finace/bill/DriverBill/index') , name: '司机账单' },
           { path: 'driverpay', component: ()=>import('@/page/Finace/bill/DriverPay/index'), name: '司机现付' },
           { path: 'drivertoPay', component:()=>import('@/page/Finace/bill/DriverToPay/index') , name: '司机到付' },
           //价格管理
           { path: 'transportprice', component: ()=>import('@/page/Finace/price/TransportPrice/index'), name: '承运价格' },
           { path: 'shippingprice', component: ()=>import('@/page/Finace/price/ShippingPrice/index'), name: '托运价格' }
     	 ],
     	 meta: { title: 'finance' }
     },
     {
     	 name: '调整',
     	 path:'/adjustment',
     	 component: Home,
     	 children:[
     	     { path: 'abnormal', component:()=>import('@/page/Adjust/Abnormal/index'), name: '送达异常'},
           { path: 'carrierbilladjust', component: ()=>import('@/page/Adjust/CarrierBillAdjust/index'), name: '承运商账单调整' },
           { path: 'customerbilladjust', component: ()=>import('@/page/Adjust/CustomerBillAdjust/index'), name: '客户账单调整' },
           { path: 'documentdeleted', component:()=>import('@/page/Adjust/DocumentDeleted/index'), name: '单据删除' },
           { path: 'log', component: ()=>import('@/page/Adjust/Log/index'), name: '操作日志' },
           { path: 'orderadjust', component: ()=>import('@/page/Adjust/OrderAdjust/index'), name: '订单调整' },
           { path: 'quickorderadjustment', component:()=>import('@/page/Adjust/QuickOrderAdjustment/index'), name: '订单快速调整' }
     	 ],
     	 meta: { title: 'adjustment' }
    },
    {
     	 name: '资源',
     	 path:'/resource',
     	 component: Home,
     	 children:[
              //基础信息
              { path: 'companys', component:()=>import('@/page/Resource/Company/Companys/index'), name: '合作公司'},
              //司机车辆
              { path: 'DriverMessage',component:()=>import
              ('@/page/Resource/Vehicle/DriverMessage/index'), name: '司机信息'},
              { path: 'VehicleMessage',component:()=>import
              ('@/page/Resource/Vehicle/VehicleMessage/index'), name: '车辆信息'},
     	 ],
     	 meta: { title: 'resource' }
    },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
  ]
})

//export default router